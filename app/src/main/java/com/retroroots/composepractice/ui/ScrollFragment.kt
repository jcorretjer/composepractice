package com.retroroots.composepractice.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import com.retroroots.composepractice.R

class ScrollFragment : Fragment()
{
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View
    {
        return ComposeView(requireContext()).apply {
            setContent {
                MainUI()
            }
        }
    }

    @Preview
    @Composable
    private fun MainUI()
    {
        MaterialTheme{

            Column(modifier = Modifier.verticalScroll(rememberScrollState())) {

                Text(text = resources.getString(R.string.scrollVwFragmentTitleText))

                    Text(text = resources.getString(R.string.complexTitleTxt), fontSize = 35.sp, modifier = Modifier.padding(all = 10.dp))

                    Text(text = resources.getString(R.string.complexItem1Txt), modifier = Modifier.padding(horizontal = 20.dp, vertical = 10.dp))

                    Text(text =  resources.getString(R.string.complexItem2Txt), modifier = Modifier.padding(horizontal = 20.dp, vertical = 10.dp))

                    Text(text =  resources.getString(R.string.complexItem3Txt), modifier = Modifier.padding(horizontal = 20.dp, vertical = 10.dp))

                    Text(text = resources.getString(R.string.complexTitleTxt), fontSize = 35.sp, modifier = Modifier.padding(all = 10.dp))

                    Text(text = resources.getString(R.string.complexItem1Txt), modifier = Modifier.padding(horizontal = 20.dp, vertical = 10.dp))

                    Text(text =  resources.getString(R.string.complexItem2Txt), modifier = Modifier.padding(horizontal = 20.dp, vertical = 10.dp))

                    Text(text =  resources.getString(R.string.complexItem3Txt), modifier = Modifier.padding(horizontal = 20.dp, vertical = 10.dp))

                    Text(text = resources.getString(R.string.complexTitleTxt), fontSize = 35.sp, modifier = Modifier.padding(all = 10.dp))

                    Text(text = resources.getString(R.string.complexItem1Txt), modifier = Modifier.padding(horizontal = 20.dp, vertical = 10.dp))

                    Text(text =  resources.getString(R.string.complexItem2Txt), modifier = Modifier.padding(horizontal = 20.dp, vertical = 10.dp))

                    Text(text =  resources.getString(R.string.complexItem3Txt), modifier = Modifier.padding(horizontal = 20.dp, vertical = 10.dp))

                    Text(text = resources.getString(R.string.complexTitleTxt), fontSize = 35.sp, modifier = Modifier.padding(all = 10.dp))

                    Text(text = resources.getString(R.string.complexItem1Txt), modifier = Modifier.padding(horizontal = 20.dp, vertical = 10.dp))

                    Text(text =  resources.getString(R.string.complexItem2Txt), modifier = Modifier.padding(horizontal = 20.dp, vertical = 10.dp))

                    Text(text =  resources.getString(R.string.complexItem3Txt), modifier = Modifier.padding(horizontal = 20.dp, vertical = 10.dp))

                    Text(text = resources.getString(R.string.complexTitleTxt), fontSize = 35.sp, modifier = Modifier.padding(all = 10.dp))

                    Text(text = resources.getString(R.string.complexItem1Txt), modifier = Modifier.padding(horizontal = 20.dp, vertical = 10.dp))

                    Text(text =  resources.getString(R.string.complexItem2Txt), modifier = Modifier.padding(horizontal = 20.dp, vertical = 10.dp))

                    Text(text =  resources.getString(R.string.complexItem3Txt), modifier = Modifier.padding(horizontal = 20.dp, vertical = 10.dp))


                Column(
                    Modifier.fillMaxSize()
                        .padding(20.dp),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    Button(onClick = {requireActivity().onBackPressed()}) {
                        Text(resources.getString(R.string.recVwFragmentTitleText))
                    }
                }
            }
        }
    }
}