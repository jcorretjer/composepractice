package com.retroroots.composepractice.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.retroroots.composepractice.R

class RecyclerViewFragment : Fragment()
{
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View
    {
        return ComposeView(requireContext()).apply {
            setContent {
                MainUI()
            }
        }
    }

    @Preview()
    @Composable
    private fun MainUI()
    {
        MaterialTheme{
            val context = LocalContext.current

            Column(modifier = Modifier.fillMaxWidth()) {

                Text(text = resources.getString(R.string.recVwFragmentTitleText))

                LazyColumn{
                    items(listOf("t1", "t2", "t3")){
                            item ->

                        Card(
                            shape = RoundedCornerShape(3.dp),
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(horizontal = 5.dp, vertical = 5.dp)
                                .clickable {
                                    Toast
                                        .makeText(context, item, Toast.LENGTH_SHORT)
                                        .show()
                                },
                            backgroundColor = Color.LightGray
                        ) {
                            Column{
                                Text(item)
                            }
                        }
                    }
                }


            }

            Column(
                modifier = Modifier.fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center) {
                Button(onClick = {findNavController().navigate(R.id.action_recyclerViewFragment_to_scrollFragment)}) {
                    Text(resources.getString(R.string.scrollVwFragmentTitleText))
                }

                Spacer(modifier = Modifier.padding(10.dp))

                Button(onClick = {requireActivity().onBackPressed()}) {
                    Text(resources.getString(R.string.toolbarFragmentTitleText))
                }
            }
        }
    }
}