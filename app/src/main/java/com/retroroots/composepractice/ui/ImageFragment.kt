package com.retroroots.composepractice.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.retroroots.composepractice.R

class ImageFragment : Fragment()
{
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View
    {
        return ComposeView(requireContext()).apply {
            setContent {
                MainUI()
            }
        }
    }

    @Preview
    @Composable
    private fun MainUI()
    {
        MaterialTheme{
            Column {
                Text(resources.getString(R.string.imageBtnText))

                Column(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalAlignment = Alignment.CenterHorizontally
                )
                {
                    Card(modifier = Modifier.size(200.dp, 200.dp)){
                        Box {
                            AsyncImage(
                                model = ImageRequest.Builder(requireContext())
                                    .data("https://shirtoid.com/wp-content/uploads/2009/09/loading.jpg")
                                    .crossfade(true)
                                    .build(),
                                placeholder = painterResource(id = R.drawable.ic_baseline_loading_24),
                                error = painterResource(id = R.drawable.ic_baseline_error_24),
                                contentScale = ContentScale.Crop,
                                contentDescription = "some image",
                                alignment = Alignment.Center,
                                modifier = Modifier.fillMaxSize()
                            )

                            Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.BottomCenter) {
                                Text(text = resources.getString(R.string.imageDesTxt), Modifier.padding(10.dp), color = Color.Red)
                            }
                        }
                    }
                }

                Column(modifier = Modifier.fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center) {
                    Button(onClick = { findNavController().navigate(R.id.action_imageFragment_to_toolbarFragment) }) {
                        Text(resources.getString(R.string.toolbarFragmentTitleText))
                    }

                    Spacer(modifier = Modifier.padding(10.dp))

                    Button(onClick = { requireActivity().onBackPressed() }) {
                        Text(resources.getString(R.string.complexBtnText))
                    }
                }
            }
        }
    }
}