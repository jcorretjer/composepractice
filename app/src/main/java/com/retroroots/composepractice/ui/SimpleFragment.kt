package com.retroroots.composepractice.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.retroroots.composepractice.R

class SimpleFragment : Fragment()
{
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View
    {
        return ComposeView(requireContext()).apply {
            setContent {
                MaterialTheme {
                    Column {
                        Text(resources.getString(R.string.simpleBtnText))

                        Text(resources.getString(R.string.simpleHello1))

                        Text(resources.getString(R.string.simpleHello2))

                        Row {
                            Text(resources.getString(R.string.simpleHello3))

                            Text(resources.getString(R.string.simpleHello4))
                        }

                        Column(
                            Modifier.fillMaxSize(),
                            horizontalAlignment = Alignment.CenterHorizontally,
                            verticalArrangement = Arrangement.Center
                        ) {
                            Button(onClick = {findNavController().navigate(R.id.action_simpleFragment_to_complexFragment)}) {
                                Text(resources.getString(R.string.complexBtnText))
                            }
                        }
                    }
                }

            }
        }
    }
}