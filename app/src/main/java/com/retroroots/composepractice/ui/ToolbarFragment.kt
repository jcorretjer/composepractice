package com.retroroots.composepractice.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Phone
import androidx.compose.material.icons.filled.Star
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.retroroots.composepractice.R

class ToolbarFragment : Fragment()
{
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View
    {
        return ComposeView(requireContext()).apply {
            setContent {
                MainUI()
            }
        }

    }

    @Preview
    @Composable
    private fun MainUI()
    {
        val context = LocalContext.current

        MaterialTheme {

            Column {
                Column(modifier = Modifier.fillMaxWidth()) {
                    TopAppBar(
                        title = {
                            Text(resources.getString(R.string.toolbarFragmentTitleText))
                        },
                        navigationIcon = {
                            IconButton(
                                onClick = {
                                    requireActivity().onBackPressed()
                                }
                            ) {
                                Icon(
                                    Icons.Filled.ArrowBack,
                                    contentDescription = "Navigate back"
                                )
                            }
                        },

                        actions = {
                            IconButton(
                                onClick = {
                                    Toast.makeText(context, resources.getString(R.string.toolbarIcon1Text), Toast.LENGTH_SHORT)
                                        .show()
                                }
                            ) {
                                Icon(
                                    Icons.Filled.Star,
                                    contentDescription = resources.getString(R.string.toolbarIcon1Text)
                                )
                            }

                            IconButton(
                                onClick = {
                                    Toast.makeText(context, resources.getString(R.string.toolbarIcon2Text), Toast.LENGTH_SHORT)
                                        .show()
                                }
                            ) {
                                Icon(
                                    Icons.Filled.Phone,
                                    contentDescription = resources.getString(R.string.toolbarIcon2Text)
                                )
                            }
                        }
                    )
                }

                Column(
                    modifier = Modifier.fillMaxSize(),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center) {
                    Button(onClick = {findNavController().navigate(R.id.action_toolbarFragment_to_recyclerViewFragment)}) {
                        Text(resources.getString(R.string.recVwFragmentTitleText))
                    }

                    Spacer(modifier = Modifier.padding(10.dp))

                    Button(onClick = {requireActivity().onBackPressed()}) {
                        Text(resources.getString(R.string.imageBtnText))
                    }
                }
            }
        }
    }

}