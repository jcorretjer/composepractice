package com.retroroots.composepractice.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.relocationRequester
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.retroroots.composepractice.R

class ComplexFragment : Fragment()
{
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View
    {
        return ComposeView(requireContext()).apply {
            setContent {
                MainUI()
            }
        }
    }

    @Preview()
    @Composable
    private fun MainUI()
    {
        MaterialTheme{
            Column {
                
                Text(text = resources.getString(R.string.complexBtnText))

                Column(
                    Modifier.fillMaxWidth(),
                    horizontalAlignment = Alignment.CenterHorizontally
                ){
                    Image(
                        painter = painterResource(R.drawable.ic_android_black_24dp),
                        contentDescription = "image",
                        contentScale = ContentScale.Crop,
                        modifier = Modifier.size(100.dp, 100.dp)
                    )

                    Divider(color = Color.Black, thickness = 1.dp)
                }
                
                Column(Modifier.fillMaxWidth() ) {
                    Text(text = resources.getString(R.string.complexTitleTxt), fontSize = 35.sp, modifier = Modifier.padding(all = 10.dp))

                    Text(text = resources.getString(R.string.complexItem1Txt), modifier = Modifier.padding(horizontal = 20.dp, vertical = 10.dp))

                    Text(text =  resources.getString(R.string.complexItem2Txt), modifier = Modifier.padding(horizontal = 20.dp, vertical = 10.dp))

                    Text(text =  resources.getString(R.string.complexItem3Txt), modifier = Modifier.padding(horizontal = 20.dp, vertical = 10.dp))
                }

                Column(
                    Modifier.fillMaxSize(),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    Button(onClick = {findNavController().navigate(R.id.action_complexFragment_to_imageFragment)}) {
                        Text(resources.getString(R.string.imageBtnText))
                    }

                    Spacer(modifier = Modifier.padding(10.dp))

                    Button(onClick = {requireActivity().onBackPressed()}) {
                        Text(resources.getString(R.string.simpleBtnText))
                    }
                }
            }
        }
    }
}